# Build stage
FROM eclipse-temurin:11-jdk-alpine AS builder
COPY .mvn .mvn
COPY mvnw .
COPY pom.xml .
COPY src src
RUN ["chmod", "+x", "mvnw"]
RUN /bin/sh ./mvnw clean package -DskipTests

# Package stage
FROM eclipse-temurin:11-jdk-alpine
COPY --from=builder target/studentsystem-0.0.1-SNAPSHOT.jar /myapp.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/myapp.jar"]
